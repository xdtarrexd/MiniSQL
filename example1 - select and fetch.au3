#include <miniSQL\miniSQL.au3>

; Declared as CONST since we never want to accidentally change the variables original value
Local Const $MySql = _miniSQL_LoadLibrary()

;Connect to database & Init library
If Not $MySql.Startup("192.168.0.10", "root", "impulse101", "test", 3306) Then
	MsgBox(0, "Failed to start library", $MySql.debug())
	Exit
EndIf


With $MySql
	.prepare("SELECT * FROM members     ")
	If Not .execute() Then MsgBox(0, "Failed to execute query", .sqlError())

	Local $oRows = .fetchAll()

	; Print how many rows got affected by latest query
	ConsoleWrite(StringFormat("Number of rows to display: %s", .rowCount()) & @CRLF)

	; we use isObj to check if we got any result.
	If IsObj($oRows) Then
		For $row In $oRows
			ConsoleWrite(StringFormat("Id: %s", $row.id) & @CRLF)
			ConsoleWrite(StringFormat("Name: %s", $row.name) & @CRLF)
			ConsoleWrite(StringFormat("Bio: %s", $row.bio) & @CRLF)
		Next
	Else
		ConsoleWrite("No rows to show"&@CRLF)
	EndIf

EndWith


; Use this in your app when you are done using the database
$MySql.Shutdown()
