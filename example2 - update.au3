#include <miniSQL\miniSQL.au3>

; Set default dir for our dlls (Only has to be done once)
_miniSQL_setDllDir(@ScriptDir & "\miniSQL")
; Declared as CONST since we never want to accidentally change the variables original value
Local Const $MySql = _miniSQL_LoadLibrary()

;Connect to database & Init library
If Not $MySql.Startup("localhost", "user", "pass", "db", 3306) Then
	MsgBox(0, "Failed to start library", $MySql.debug())
	Exit
EndIf

With $MySql
	; We use an array to make our query look nicer
	Local $vars = [":name", @UserName&Random(1,10,1)]

	; Prepare our statement
	.prepare("UPDATE members SET name = :name WHERE 1")
	If Not .execute($vars) Then MsgBox(0, "Failed to execute query", .sqlError())

	; Print how many rows got affected by latest query
	ConsoleWrite(StringFormat("Example 1 rows affected: %s", .rowCount()) & @CRLF)
EndWith

; We can also prepare like this

With $MySql
	Local $vars = ["?", @UserName, "?", 1]

	; Prepare our statement
	.prepare("UPDATE members SET name = ? WHERE id = ?")
	If Not .execute($vars) Then MsgBox(0, "Failed to execute query", .sqlError())

	; Print how many rows got affected by latest query
	ConsoleWrite(StringFormat("Example 2 rows affected: %s", .rowCount()) & @CRLF)
EndWith


; Use this in your app when you are done using the database
$MySql.Shutdown()
