<pre>
;#
;#         _     _ _____ _____ __
;#   _____|_|___|_|   __|     |  |
;#  |     | |   | |__   |  |  |  |__
;#  |_|_|_|_|_|_|_|_____|__  _|_____|
;#                         |__|
;# By tarretarretarre
;# Repository: https://gitlab.com/xdtarrexd/MiniSQL
;# Official Topic: https://www.autoitscript.com/forum/topic/180415-minisql-a-simple-pdo-syntax-like-mysql-library-with-autoitobject/
;# Last update: (see git)
;#
;#  ~  ~  ~  ~  Big thanks to  ~  ~  ~  ~  ~  ~
;#  ~ ProgAndy & the whole AutoitObject team  ~
;#  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~
;#
;# Inspired & Used code from "MySQL UDFs (without ODBC)" Used https://www.autoitscript.com/forum/topic/85617-mysql-udfs-without-odbc/
;# AutoitObject used for objects "" https://www.autoitscript.com/forum/topic/110379-autoitobject-udf/
</pre>