#include-once
#include <libsql.dll.au3>
#cs
	;/* Copyright (C) 2000-2003 MySQL AB

	This program is free software; you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation; version 2 of the License.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program; if not, write to the Free Software
	Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA */

	;/*
	This file defines the client API to MySQL and also the ABI of the
	dynamically linked libmysqlclient.

	The ABI should never be changed in a released product of MySQL
	thus you need to take great care when changing the file. In case
	the file is changed so the ABI is broken, you must also
	update the SHAREDLIB_MAJOR_VERSION in configure.in .

	*/
#ce
;===================================================================================
;
; Original Script:   MySQL UDFs working with libmysql.dll
; Original Version:  1.0.0.2
; Original Author:   Prog@ndy
; Modified by:		 tarretarretarre
;
; YOU ARE NOT ALLOWED TO REMOVE THIS INFORMATION
;===================================================================================

Global Const $__MySQL_LoadedLibraryFileX32 = "libMySQL_2016_x32.dll", $__MySQL_LoadedLibraryFileX64 = "libMySQL_2016_x64.dll"
Global Const $__MySQL_DefaultDLLDIR = @TempDir
Local Const $MYSQL_FIELD = _
		"ptr name;" & _                 ;/* Name of column */ [[char *
		"ptr orgName;" & _             ;/* Original column name, if an alias */ [[char *
		"ptr table;" & _                ;/* Table of column if column was a field */ [[char *
		"ptr orgTable;" & _            ;/* Org table name, if table was an alias */ [[char *
		"ptr db;" & _                   ;/* Database for table */ [[char *
		"ptr catalog;" & _	      ;/* Catalog for table */ [[char *
		"ptr def;" & _                  ;/* Default value (set by mysql_list_fields) */ [[char *
		"ulong length;" & _       ;/* Width of column (create length) */
		"ulong maxLength;" & _   ;/* Max width for selected set */
		"uint nameLength;" & _
		"uint orgNameLength;" & _
		"uint tableLength;" & _
		"uint orgTableLength;" & _
		"uint dbLength;" & _
		"uint catalogLength;" & _
		"uint defLength;" & _
		"uint flags;" & _         ;/* Div flags */
		"uint decimals;" & _      ;/* Number of decimals in field */
		"uint charsetnr;" & _     ;/* Character set */
		"int type;" & _ ;/* Type of field. See mysql_com.h for types */
		"ptr extension;"

Local $__MySQL_LoadedLibrary = False


Func _MySQL_errno($MySQL_ptr)
	If Not $MySQL_ptr Then Return SetError(3, 0, 0)
	Local $errors = DllCall($__MySQL_LoadedLibrary, "uint", "mysql_errno", "ptr", $MySQL_ptr)
	If @error Then Return SetError(1, 0, 0)
	Return $errors[0]
EndFunc   ;==>_MySQL_errno

Func _MySQL_Close($MySQL_ptr)
	If Not $MySQL_ptr Then Return SetError(3, 0, 0)
	DllCall($__MySQL_LoadedLibrary, "none", "mysql_close", "ptr", $MySQL_ptr)
	DllClose($__MySQL_LoadedLibrary)
	$__MySQL_LoadedLibrary = False
	If @error Then Return SetError(1, 0, 0)
	FileDelete(StringFormat("%s\%s", $__MySQL_DefaultDLLDIR, $__MySQL_LoadedLibraryFileX64))
	FileDelete(StringFormat("%s\%s", $__MySQL_DefaultDLLDIR, $__MySQL_LoadedLibraryFileX32))
	Return 1
EndFunc   ;==>_MySQL_Close

Func _MySQL_init($sForceLib = False)
	;Prevent double INIT
	If $__MySQL_LoadedLibrary Then Return SetError(1, 0, 0)
	Local $MySQL_ptr, $__DLL

	;Create files if they dont exist (in tmp dir) (Will be removed when closing app)
	_LibSqlDLL_x32(True, @TempDir)
	_LibSqlDLL_x64(True, @TempDir)


	; Detect environment (If not forced)
	If Not $sForceLib Then
		If @AutoItX64 Then
			$__DLL = StringFormat("%s\%s", $__MySQL_DefaultDLLDIR, $__MySQL_LoadedLibraryFileX64)
		Else
			$__DLL = StringFormat("%s\%s", $__MySQL_DefaultDLLDIR, $__MySQL_LoadedLibraryFileX32)
		EndIf
	Else
		$__DLL = $sForceLib
	EndIf

	If Not FileExists($__DLL) Then Return SetError(2, 0, 0)

	; Open DLL
	$__MySQL_LoadedLibrary = DllOpen($__DLL)

	; create socket
	$MySQL_ptr = DllCall($__MySQL_LoadedLibrary, "ptr", "mysql_init", "ptr", 0); MySQL_server_init is invoked here so we dont need to call it
	If @error Then Return SetError(3, 0, 0)

	Return $MySQL_ptr[0]
EndFunc   ;==>_MySQL_init

Func _MySQL_real_connect($MySQL_ptr, $sHost, $sUser, $sPass, $sDatabase = "", $iPort = 0, $sUnix_socket = "", $iClient_Flag = 0)
	If Not $MySQL_ptr Then Return SetError(3, 0, 0)

	Local $PWType = "str", $DBType = "str", $UXSType = "str"

	If $sPass = "" Then $PWType = "ptr"
	If $sDatabase = "" Then $DBType = "ptr"
	If $sUnix_socket = "" Then $UXSType = "ptr"

	Local $conn = DllCall($__MySQL_LoadedLibrary, "ptr", "mysql_real_connect", "ptr", $MySQL_ptr, "str", $sHost, "str", $sUser, $PWType, $sPass, $DBType, $sDatabase, "uint", $iPort, $UXSType, $sUnix_socket, "ulong", $iClient_Flag)

	If @error Then Return SetError(1, 0, 0)

	Return $conn[0]
EndFunc   ;==>_MySQL_real_connect

Func _MySQL_Real_Query($MySQL_ptr, $sQuerystring)
	Local $querystringlength = StringLen($sQuerystring)
	DllCall($__MySQL_LoadedLibrary, "int", "mysql_real_query", "ptr", $MySQL_ptr, "str", $sQuerystring, "ulong", $querystringlength)
EndFunc   ;==>_MySQL_Real_Query

Func _MySQL_Insert_ID($MySQL_ptr)
	If Not $MySQL_ptr Then Return SetError(3, 0, 0)
	Local $row = DllCall($__MySQL_LoadedLibrary, "uint64", "mysql_insert_id", "ptr", $MySQL_ptr)
	If @error Then Return SetError(1, 0, 0)
	Return $row[0]
;~ 	Return __MySQL_ReOrderULONGLONG($row[0])
EndFunc   ;==>_MySQL_Insert_ID

Func _MySQL_Error($MySQL_ptr)
	Local $errors = DllCall($__MySQL_LoadedLibrary, "str", "mysql_error", "ptr", $MySQL_ptr)
	If @error Then Return SetError(1, 0, "")
	Return $errors[0]
EndFunc   ;==>_MySQL_Error

Func _MySQL_Store_Result($MySQL_ptr)
	Local $ret = DllCall($__MySQL_LoadedLibrary, "ptr", "mysql_store_result", "ptr", $MySQL_ptr)
	Return $ret[0]
EndFunc   ;==>_MySQL_Store_Result

Func _MySQL_Fetch_Result_StringArray($result, $NULLasPtr0 = False)
	_MySQL_Data_Seek($result, 0)
	Local $fields = _MySQL_Num_Fields($result)
	Local $rows = _MySQL_Num_Rows($result)
	If $fields < 1 Or $rows < 1 Then Return SetError(1, 0, 0)
	Local $ResultArr[$rows + 1][$fields]
	Local $Names = _MySQL_Fetch_Fields_Names($result, $fields)
	For $i = 0 To $fields - 1
		$ResultArr[0][$i] = $Names[$i]
	Next
	Local $length, $fieldPtr, $rowPtr, $mysqlrow, $lengths, $lenthsStruct
	Local Const $NULLPTR = Ptr(0)
	For $j = 1 To $rows
		$mysqlrow = _MySQL_Fetch_Row($result, $fields)
		If Not IsDllStruct($mysqlrow) Then ExitLoop

		$lenthsStruct = _MySQL_Fetch_Lengths($result)

		For $i = 1 To $fields
			$length = DllStructGetData($lenthsStruct, 1, $i)
			$fieldPtr = DllStructGetData($mysqlrow, 1, $i)
			Select
				Case $length ; if there is data
					$ResultArr[$j][$i - 1] = DllStructGetData(DllStructCreate("char[" & $length & "]", $fieldPtr), 1)
				Case $NULLasPtr0 And Not $fieldPtr ; is NULL and return NULL as Ptr(0)
					$ResultArr[$j][$i - 1] = $NULLPTR
			EndSelect
		Next
	Next
	Return $ResultArr
EndFunc   ;==>_MySQL_Fetch_Result_StringArray

Func _MySQL_Data_Seek($result, $offset)
	If Not $result Then Return SetError(3, 0, 0)
	DllCall($__MySQL_LoadedLibrary, "none", "mysql_data_seek", "ptr", $result, "uint64", $offset)
	If @error Then Return SetError(1, 0, 0)
	Return 1
EndFunc   ;==>_MySQL_Data_Seek

Func _MySQL_Fetch_Lengths($result, $numberOfFields = Default)
	If $numberOfFields = Default Then $numberOfFields = _MySQL_Num_Fields($result)
	If $numberOfFields <= 0 Then Return SetError(1, 0, 0)

	Local $lengths = DllCall($__MySQL_LoadedLibrary, "ptr", "mysql_fetch_lengths", "ptr", $result)
	If @error Then Return SetError(1, 0, 0)

	Return DllStructCreate("ulong lengths[" & $numberOfFields & "]", $lengths[0])
EndFunc   ;==>_MySQL_Fetch_Lengths

Func _MySQL_Num_Fields($result)
	If Not $result Then Return SetError(1, 0, 0)
	Local $row = DllCall($__MySQL_LoadedLibrary, "uint", "mysql_num_fields", "ptr", $result)
	If @error Then Return SetError(1, 0, 0)
	Return $row[0]
EndFunc   ;==>_MySQL_Num_Fields

Func _MySQL_Free_Result($result)
	DllCall($__MySQL_LoadedLibrary, "none", "mysql_free_result", "ptr", $result)
EndFunc   ;==>_MySQL_Free_Result

Func _MySQL_Num_Rows($result)
	If Not $result Then Return SetError(3, 0, 0)
	Local $rows = DllCall($__MySQL_LoadedLibrary, "uint64", "mysql_num_rows", "ptr", $result)
	If @error Then Return SetError(1, 0, 0)
	Return $rows[0]
;~ 	Return __MySQL_ReOrderULONGLONG($rows[0])
EndFunc   ;==>_MySQL_Num_Rows

Func _MySQL_Fetch_Fields_Names($result, $numberOfFields = Default)
	If $numberOfFields = Default Then $numberOfFields = _MySQL_Num_Fields($result)
	If $numberOfFields = 0 Then Return SetError(1, 0, 0)
	Local $fields = DllCall($__MySQL_LoadedLibrary, "ptr", "mysql_fetch_fields", "ptr", $result)
	If @error Then Return SetError(1, 0, 0)
	$fields = $fields[0]
	Local $struct = DllStructCreate($MYSQL_FIELD, $fields)
	Local $arFields[$numberOfFields]
	For $i = 1 To $numberOfFields
		$arFields[$i - 1] = __MySQL_PtrStringRead(DllStructGetData($struct, 1))
		If $i = $numberOfFields Then ExitLoop
		$struct = DllStructCreate($MYSQL_FIELD, $fields + (DllStructGetSize($struct) * $i))
	Next
	Return $arFields
EndFunc   ;==>_MySQL_Fetch_Fields_Names

Func _MySQL_Fetch_Row($result, $numberOfFields = Default)
	If $numberOfFields = Default Then $numberOfFields = _MySQL_Num_Fields($result)
	If $numberOfFields <= 0 Then Return SetError(1, 0, 0)

	Local $row = DllCall($__MySQL_LoadedLibrary, "ptr", "mysql_fetch_row", "ptr", $result)
	If @error Then Return SetError(1, 0, 0)

	Return DllStructCreate("ptr[" & $numberOfFields & "]", $row[0])
EndFunc   ;==>_MySQL_Fetch_Row

Func _MySQL_Affected_Rows($MySQL_ptr)
	If Not $MySQL_ptr Then Return SetError(3, 0, -1)
	Local $row = DllCall($__MySQL_LoadedLibrary, "uint64", "mysql_affected_rows", "ptr", $MySQL_ptr)
	If @error Then Return SetError(1, 0, -1)
	Return $row[0]

EndFunc   ;==>_MySQL_Affected_Rows

Func __MySQL_PtrStringRead($ptr, $IsUniCode = False, $StringLen = -1)
	Local $UniCodeString = ""
	If $IsUniCode Then $UniCodeString = "W"
	If $StringLen < 1 Then $StringLen = __MySQL_PtrStringLen($ptr, $IsUniCode)
	If $StringLen < 1 Then Return SetError(1, 0, "")
	Local $struct = DllStructCreate($UniCodeString & "char[" & ($StringLen + 1) & "]", $ptr)
	Return DllStructGetData($struct, 1)
EndFunc   ;==>__MySQL_PtrStringRead

Func __MySQL_PtrStringLen($ptr, $IsUniCode = False)
	Local $UniCodeFunc = ""
	If $IsUniCode Then $UniCodeFunc = "W"
	Local $ret = DllCall("kernel32.dll", "int", "lstrlen" & $UniCodeFunc, "ptr", $ptr)
	If @error Then Return SetError(1, 0, -1)
	Return $ret[0]
EndFunc   ;==>__MySQL_PtrStringLen
