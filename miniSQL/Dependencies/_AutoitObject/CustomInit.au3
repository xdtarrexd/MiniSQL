#include-once
#include "AutoItObject.au3"
#include "LinkedList.au3"

Local $__LatestCOMERROR

#Region Error
; Error monitoring
Local $oError

Func __CI_DisableError()
	$oError = Null
	Return ObjEvent("AutoIt.Error", "__NoErr")
EndFunc   ;==>__CI_DisableError

Func __CI_EnableError()
	$oError = ObjEvent("AutoIt.Error", "__ErrFunc")
EndFunc   ;==>__CI_EnableError

Func __ErrFunc($oError)
	$__LatestCOMERROR = StringFormat('! COM Error ! Number: 0x%d, ScriptLine: %d, Reason: %s', Hex($oError.number, 8), $oError.scriptline, $oError.windescription) & @CRLF
	ConsoleWrite($__LatestCOMERROR)
	Return
EndFunc   ;==>__ErrFunc

Func __NoErr()
	Return
EndFunc   ;==>__NoErr
#EndRegion Error

#Region AutoitObject startup
__CI_EnableError()
_AutoItObject_Startup()
#EndRegion AutoitObject startup
