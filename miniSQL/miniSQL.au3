;#
;#         _     _ _____ _____ __
;#   _____|_|___|_|   __|     |  |
;#  |     | |   | |__   |  |  |  |__
;#  |_|_|_|_|_|_|_|_____|__  _|_____|
;#                         |__|
;# By tarretarretarre
;# Repository: https://gitlab.com/xdtarrexd/MiniSQL
;# Last update: (see git)
;#
;#  ~  ~  ~  ~  Big thanks to  ~  ~  ~  ~  ~  ~
;#  ~ ProgAndy & the whole AutoitObject team  ~
;#  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~  ~
;#
;# Inspired & Used code from "MySQL UDFs (without ODBC)" Used https://www.autoitscript.com/forum/topic/85617-mysql-udfs-without-odbc/
;# AutoitObject used for objects "" https://www.autoitscript.com/forum/topic/110379-autoitobject-udf/
#include-once
#include <Dependencies\_AutoitObject\CustomInit.au3>
#include <Dependencies\_MySQL\MySQL.au3>


Local Const $_miniSQL_Version = 1.1
Local Enum $_miniSQL_FetchObject, $_miniSQL_FetchSingleObject, $_miniSQL_FetchArray, $_miniSQL_FetchSingleValue
Local Enum $_miniSQL_ExecuteStoreResult, $_miniSQL_ExecuteOnly

Func _miniSQL_LoadLibrary()

	; MySql Standard objects
	Local $ClassObj = _AutoItObject_Class()

	$ClassObj.AddProperty("isInited", $ELSCOPE_PRIVATE, False)
	$ClassObj.AddProperty("MySQL_PTR", $ELSCOPE_PRIVATE, Null)

	; Resources
	$ClassObj.AddProperty("sQuery", $ELSCOPE_PRIVATE, Null)
	$ClassObj.AddProperty("sFinalQuery", $ELSCOPE_PRIVATE, Null)

	$ClassObj.AddProperty("StoredResult", $ELSCOPE_PRIVATE, Null) ; ARRAY

	; Methods
	$ClassObj.AddMethod("startup", "_miniSQL_Startup")
	$ClassObj.AddMethod("shutdown", "_miniSQL_Shutdown")

	$ClassObj.AddMethod("prepare", "_miniSQL_Prepare")
	$ClassObj.AddMethod("prepareGlue", "_miniSQL_PrepareGlue")
	$ClassObj.AddMethod("prepareClean", "_miniSQL_PrepareClean")

	$ClassObj.AddMethod("execute", "_miniSQL_Execute")
	$ClassObj.AddMethod("fetchAll", "_miniSQL_fetchAll")

	; Handy methods
	$ClassObj.AddMethod("rowCount", "_miniSQL_rowCount")
	$ClassObj.AddMethod("lastInsertId", "_MySQL_lastInsertId")
	$ClassObj.AddMethod("debug", "_miniSQL_debug")
	$ClassObj.AddMethod("sqlError", "_miniSQL_SQLerror")

	; Return the object
	Return $ClassObj.Object
EndFunc   ;==>_miniSQL_LoadLibrary

#Region Startup and Shutdown
Func _miniSQL_Startup($this, $sHost, $sUser, $sPass, $sDatabase = "", $iPort = 0, $sUnix_socket = "", $iClient_Flag = 0)
	If $this.isInited Then Return SetError(1, 0, 0)
	Local $MySQL_ptr, $MySql_conn, $__SUCCESS = True

	$MySQL_ptr = _MySQL_init()

	If Not $MySQL_ptr Then $__SUCCESS = False

	$MySql_conn = _MySQL_real_connect($MySQL_ptr, $sHost, $sUser, $sPass, $sDatabase, $iPort, $sUnix_socket, $iClient_Flag)

	If Not $MySql_conn Then $__SUCCESS = False

	$this.MySQL_PTR = $MySQL_ptr
	$this.sQuery = Null
	$this.sFinalQuery = Null
	$this.StoredResult = Null

	if $__SUCCESS Then
		$this.isInited = True
		Return $__SUCCESS
	EndIf


EndFunc   ;==>_miniSQL_Startup

Func _miniSQL_Shutdown($this)
	_MySQL_Close($this.MySQL_PTR)
	$this.isInited = False
EndFunc   ;==>_miniSQL_Shutdown
#EndRegion Startup and Shutdown

#Region Prepared statements
Func _miniSQL_Prepare($this, $sQuery)
	$this.sQuery = $sQuery
	$this.sFinalQuery = $sQuery
EndFunc   ;==>_miniSQL_Prepare

Func _miniSQL_PrepareGlue($this, $sQuery)
	If $this.sQuery <> "" Then
		If StringRight($sQuery, 1) == " " Then
			$this.sQuery &= $sQuery
		Else
			$this.sQuery &= $sQuery & " "
		EndIf
	EndIf
EndFunc   ;==>_miniSQL_PrepareGlue

Func _miniSQL_PrepareClean($this)
	$this.sQuery = Null
EndFunc   ;==>_miniSQL_PrepareClean
#EndRegion Prepared statements

Func _miniSQL_Execute($this, $aVars = Null, $iExecuteStyle = $_miniSQL_ExecuteStoreResult)

	If Not $this.isInited Then Return False

	Local $FinalQuery = $this.sQuery
	Local $Numparams = UBound($aVars);
	Local $SprintfMode = 0

	; Check if SprintF is going to be used
	If StringRegExp($FinalQuery, "(?)") Then
		$SprintfMode = 1
	EndIf

	If IsArray($aVars) And Mod($Numparams, 2) <> 0 Then
		Return False
	Else
		For $i = 0 To $Numparams Step +2

			If $i + 1 > $Numparams Then ExitLoop

			Local $SearchString = $aVars[$i]
			Local $ReplaceString = StringFormat("'%s'", $aVars[$i + 1])

			$FinalQuery = StringReplace($FinalQuery, $SearchString, $ReplaceString, $SprintfMode, 1)
		Next
	EndIf

	; Store the final query for debugging
	$this.sFinalQuery = $FinalQuery

	; Execute final query
	_MySQL_Real_Query($this.MySQL_PTR, $FinalQuery)

	Switch $iExecuteStyle
		Case $_miniSQL_ExecuteStoreResult
			; Store the result in mysql lib
			Local $aResult = _MySQL_Store_Result($this.MySQL_PTR)

			; Fetch array and store it in miniSQL
			$this.StoredResult = _MySQL_Fetch_Result_StringArray($aResult)
		Case $_miniSQL_ExecuteOnly
			; We do nothing
	EndSwitch

	; Free the result
	_MySQL_Free_Result($aResult)

	; Any errors?
	if _MySQL_Error($this.MySQL_PTR) Then Return False

	; We made it
	Return True
EndFunc   ;==>_miniSQL_Execute

Func _miniSQL_fetchAll($this, $iFetchStyle = $_miniSQL_FetchObject)

	If Not $this.isInited Then Return False

	; We store the array back from AO to Autoit bcuz accesing arrays through AO is slower (a tiny bit, but every bit count right?)
	Local $StoredResult = $this.StoredResult

	; No result ? return false
	If Not IsArray($StoredResult) Then Return False

	; Return the data in the desired style
	Switch $iFetchStyle

		Case $_miniSQL_FetchArray
			Return $StoredResult
		Case $_miniSQL_FetchSingleValue
			Return $StoredResult[1][0]
		Case $_miniSQL_FetchSingleObject
			Local $oRowObject = _AutoItObject_Create()
			_AutoItObject_AddProperty($oRowObject, $StoredResult[0][0], $ELSCOPE_PUBLIC, $StoredResult[1][0])
			Return $oRowObject
		Case $_miniSQL_FetchObject

			Local $uCols = UBound($StoredResult, 2)
			Local $uRows = UBound($StoredResult, 1)

			Local $oListRet = _AutoitObject_LinkedList()

			For $i = 1 To $uRows - 1

				Local $oRowObject = _AutoItObject_Create()

				For $j = 0 To $uCols - 1

					Local $RowName = $StoredResult[0][$j]
					Local $RowData = $StoredResult[$i][$j]

					_AutoItObject_AddProperty($oRowObject, $RowName, $ELSCOPE_PUBLIC, $RowData)

				Next

				$oListRet.add($oRowObject)
			Next

			Return $oListRet
	EndSwitch
EndFunc   ;==>_miniSQL_fetchAll

#Region Misc Methods
Func _miniSQL_debug($this); anti-headache method for anything
	Local $sDebug = ""

	$sDebug &= StringFormat("--> ###   miniSQL v%d. Debug dump   ###", $_miniSQL_Version) & @CRLF & @CRLF

	$sDebug &= "--> ~ Basic information" & @CRLF
	$sDebug &= StringFormat("--> Is the library active?: %s", $this.isInited) & @CRLF
	$sDebug &= StringFormat("--> Do we have any errnos?: %s", _MySQL_errno($this.MySQL_PTR)) & @CRLF &@CRLF

	$sDebug &= "--> ~ Query information" & @CRLF
	$sDebug &= StringFormat("--> Input query  : %s", $this.sQuery) & @CRLF
	$sDebug &= StringFormat("--> Output query : %s", $this.sFinalQuery) & @CRLF &@CRLF

	$sDebug &= StringFormat("--> Mysql error  : %s", _MySQL_Error($this.MySQL_PTR)) & @CRLF

	ConsoleWrite(@CRLF&$sDebug&@CRLF)

	Return $sDebug
EndFunc   ;==>_miniSQL_debug

Func _miniSQL_SQLerror($this)
	If Not $this.isInited Then Return False
	Return _MySQL_Error($this.MySQL_PTR)
EndFunc

Func _miniSQL_lastInsertId($this)
	If Not $this.isInited Then Return False
	Return _MySQL_Insert_ID($this.MySQL_PTR)
EndFunc   ;==>_miniSQL_lastInsertId

Func _miniSQL_rowCount($this)
	If Not $this.isInited Then Return False
	Return _MySQL_Affected_Rows($this.MySQL_PTR)
EndFunc   ;==>_miniSQL_rowCount
#EndRegion Misc Methods
